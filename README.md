# NGINX HEROKU BUILDPACK

Nginx-buildpack vendors NGINX inside a dyno and connects NGINX to an app server via UNIX domain sockets.

## Motivation

Some application servers (e.g. Ruby's Unicorn) halt progress when dealing with network I/O. Heroku's Cedar routing stack [buffers only the headers](https://devcenter.heroku.com/articles/http-routing#request-buffering) of inbound requests. (The Cedar router will buffer the headers and body of a response up to 1MB) Thus, the Heroku router engages the dyno during the entire body transfer –from the client to dyno. For applications servers with blocking I/O, the latency per request will be degraded by the content transfer. By using NGINX in front of the application server, we can eliminate a great deal of transfer time from the application server. In addition to making request body transfers more efficient, all other I/O should be improved since the application server need only communicate with a UNIX socket on localhost. Basically, for webservers that are not designed for efficient, non-blocking I/O, we will benefit from having NGINX to handle all I/O operations.

## Versions

- Buildpack Version: 1.0
- NGINX Version: 1.14.0

**Included Nginx Modules:**

- headers-more-nginx-module
- http_gzip_static_module
- stream_ssl_module
- poll_module
- http_v2_module
- http_realip_module

## Requirements

- Your webserver listens to the socket at `/tmp/nginx.socket`.
- You touch `/tmp/app-initialized` when you are ready for traffic.
- You can start your web server with a shell command.

## Features

- Unified NXNG/App Server logs.
- [L2met](https://github.com/ryandotsmith/l2met) friendly NGINX log format.
- [Heroku request ids](https://devcenter.heroku.com/articles/http-request-id) embedded in NGINX logs.
- Crashes dyno if NGINX or App server crashes. Safety first.
- Language/App Server agnostic.
- Customizable NGINX config.
- Application coordinated dyno starts.

### Logging

NGINX will output the following style of logs:

```bash
measure.nginx.service=0.007 request_id=e2c79e86b3260b9c703756ec93f8a66d
```

You can correlate this id with your Heroku router logs:

```bash
at=info method=GET path=/ host=salty-earth-7125.herokuapp.com request_id=e2c79e86b3260b9c703756ec93f8a66d fwd="67.180.77.184" dyno=web.1 connect=1ms service=8ms status=200 bytes=21
```

### Language/App Server Agnostic

Nginx-buildpack provides a command named `bin/start-nginx` this command takes another command as an argument. You must pass your app server's startup command to `start-nginx`.

For example, to get NGINX and Unicorn up and running:

```bash
$ cat Procfile
web: bin/start-nginx bundle exec unicorn -c config/unicorn.rb
```

### Setting the Worker Processes

You can configure NGINX's `worker_processes` directive via the
`NGINX_WORKERS` environment variable.

For example, to set your `NGINX_WORKERS` to 8 on a PX dyno:

```bash
$ heroku config:set NGINX_WORKERS=8
```

### Customizable NGINX Config

You can provide your own NGINX config by creating a file named `nginx.conf.erb` in the config directory of your app.

### Customizable NGINX Compile Options

See [scripts/build_nginx](scripts/build_nginx) for the build steps. Configuring is as easy as changing the "./configure" options.

You can run the builds in a [Docker](https://www.docker.com/) container:

```
$ docker-machine create --driver virtualbox cedar
$ eval "$(docker-machine env cedar)"
$ make build # It outputs the latest builds to bin/cedar-*
```

To test the builds:

```
$ make shell
$ cp bin/nginx-$STACK bin/nginx
$ FORCE=1 bin/start-nginx
```

### Application/Dyno coordination

The buildpack will not start NGINX until a file has been written to `/tmp/app-initialized`. Since NGINX binds to the dyno's $PORT and since the $PORT determines if the app can receive traffic, you can delay NGINX accepting traffic until your application is ready to handle it. The examples below show how/when you should write the file when working with Unicorn.

### Updating and bundling buildpack On Windows

1.) We will use Docker toolbox and Virtual Box. If we are one Windows other then Windows Home we will have Hyper V enabled.
We need to turn it off and restart the computer to use Docker toolbox.
2.) Start Docker Quickstart terminal and navigate to **this buildpack folder** folder.
3.) To list all Docker machines we run `docker-machine ls`

    - to stop the machine `docker-machine stop <name>`
    - to remove machine `docker-machine rm <name>` (leave the default one)
    - list all containers `docker ps -a`
    - list all running containers `docker ps`
    - stop all containers `docker stop $(docker ps -a -q)`
    - remove all containers `docker rm $(docker ps -a -q)`

4.) If there is no **cedar** currently running (we can also see that inside Virtual box GUI, there is the **default** one and **cedar**) we run `docker-machine create --driver virtualbox cedar`
5.) After that we run `docker-machine env cedar`.
6.) On the last line it will says "Run this command to configure your shell". We just **copy and run it**.
7.) We make all the changes that we want inside buildpack.
8.) After that we run `make build`. This will output the latest build to **bin** folder
9.) We add it, commit it and push it to repository.

**Deploying to Heroku**
When we update buildpack there are still cached files in Heroku CACHE_DIR. We should clean them before deploying new buildpack.
We run:
1.) `heroku plugins:install heroku-repo` This is just the first time to instal plugin for CLI. How to add, remove, list and update this CLI plugins we can see here. <https://devcenter.heroku.com/articles/using-cli-plugins>
2.) `heroku repo:purge_cache -a <appname>`

#### Making Changes in the buildpack

**Changing NGINX version**
For Heroku-18 stack we change it inside **Makefile**. For other stacks we change it inside **build_nginx** file.
This is the default version.

**Adding NGINX Modules**
We add them inside 'scripts/build_nginx' under './configure'. Add it at the end. Important thing is that all lines end with `\` except the last line.
This modules will be build on Heroku dyno. That is why when we deploy to Heroku with NGINX we first deploy `web: scripts/build_nginx.sh`
inside of `Procfile`. That is when he will build and instal NGINX inside of `bin` folder. We can see it by running `heroku run bash`.

If we are on Windows maybe `make build` will not work. We should follow this instructions to enable it.
1.) Go to ezwinports, <https://sourceforge.net/projects/ezwinports/files/>
2.) Download **make-4.2.1-without-guile-w32-bin.zip** (get the version without guile)
3.) Extract zip
4.) Copy the contents to 'C:\ProgramFiles\Git\mingw64\' merging the folders, but do **NOT overwrite/replace** any existing files. Basically we just copy and paste all the folders without overwriting and they will merge.

**Upgrading Heroku to the latest heroku-18 stack**
<https://devcenter.heroku.com/articles/upgrading-to-the-latest-stack>
Just be careful to use **heroku-18** where it says heroku-16.
